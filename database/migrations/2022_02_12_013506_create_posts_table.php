<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            /*
            * @Comentario:
                * Usualmente práctica que más he visto y plasmada es colocar el nombre de la tabla foranea en singular.
                * Suelo colocar las llaves foreaneas debajo de la llave primaria (Al inicio) para llevar una secuencia
                    y dejar los campos de seguimiento al final (Datos de creación y actualización).
            */
            $table->unsignedBigInteger('categorias_id');
            $table->string('titulo', 150);
            $table->text('contenido');
            $table->binary('estado')->default(1);
            $table->binary('anulado')->default(0);

            /*
            * @Comentario:
               * Preferí quitar el manejo de la creación y actualización automática que trae Laravel para tomar
                    los campos plasmados en la prueba.
            */
            $table->timestamp('fecha_creacion')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('fecha_actualizacion')->nullable();

            $table->foreign('categorias_id')->references('id')->on('categorias');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
