<?php

namespace Database\Factories;

use App\Models\Post;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{
    protected $model = Post::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $minCategoriaId = DB::table('categorias')->min('id');
        $maxCategoriaId = DB::table('categorias')->max('id');

        return [
            'categorias_id' => $this->faker->numberBetween($minCategoriaId, $maxCategoriaId),
            'titulo' => $this->faker->sentence(5),
            'contenido' => $this->faker->sentence(30),
            'estado' => $this->faker->numberBetween(0, 1),
            'anulado' => $this->faker->numberBetween(0, 1)
        ];
    }
}
