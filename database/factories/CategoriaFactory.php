<?php

namespace Database\Factories;

use App\Models\Categoria;
use Illuminate\Database\Eloquent\Factories\Factory;

class CategoriaFactory extends Factory
{
    protected $model = Categoria::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nombre' => $this->faker->sentence(5),
            'estado' => $this->faker->numberBetween(0, 1),
            'anulado' => $this->faker->numberBetween(0, 1)
        ];
    }
}
