<?php

namespace Database\Factories;

use App\Models\Comentario;
use App\Models\Post;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\DB;

class ComentarioFactory extends Factory
{
    protected $model = Comentario::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $minPostId = DB::table('posts')->min('id');
        $maxPostId = DB::table('posts')->max('id');

        return [
            'posts_id' => $this->faker->numberBetween($minPostId, $maxPostId),
            'contenido' => $this->faker->sentence(20),
            'estado' => $this->faker->numberBetween(0, 1),
            'anulado' => $this->faker->numberBetween(0, 1)
        ];
    }
}
