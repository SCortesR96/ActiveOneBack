<?php

namespace App\Http\Controllers;

use App\Models\Comentario;
use Illuminate\Http\Request;
use App\Services\ComentarioService;
use App\Services\Shared\LogService;
use App\Services\Shared\JSONService;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Response as HttpResponse;

class ComentarioController extends Controller
{
    private JSONService $_jsonService;
    private LogService $_logService;
    private ComentarioService $_comentarioService;

    public function __construct()
    {
        $this->_jsonService = new JSONService();
        $this->_logService = new LogService();
        $this->_comentarioService = new ComentarioService();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->_comentarioService->ListarComentarios();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [];

        $rules = [
            'posts_id' => 'required|integer|min:1',
            'contenido' => 'required|string|max:250',
        ];

        $messages = [
            'required' => 'La información del campo ":attribute" es requerida.',
            'max' => 'El campo ":attribute" no puede excender la cantidad de cáracteres especificada'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $data = [
                'errors' => $validator->errors()->all()
            ];

            return $this->jsonReponse = $this->_jsonService->JSONResponse(
                false,
                '¡Por favor, verificar la información ingresada!',
                HttpResponse::HTTP_BAD_REQUEST,
                $data
            );
        }

        return $this->_comentarioService->CrearComentario($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Comentario  $comentario
     * @return \Illuminate\Http\Response
     */
    public function show(Comentario $comentario)
    {
        return $this->_comentarioService->MostrarComentario($comentario);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Comentario  $comentario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comentario $comentario)
    {
        $data = [];

        $rules = [
            'posts_id' => 'required|integer|min:1',
            'contenido' => 'required|string|max:250',
            'estado' => 'required|integer|min:0|max:1'
        ];

        $messages = [
            'required' => 'La información del campo ":attribute" es requerida.',
            'max' => 'El campo ":attribute" no puede excender la cantidad de cáracteres especificada'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $data = [
                'errors' => $validator->errors()->all()
            ];

            return $this->jsonReponse = $this->_jsonService->JSONResponse(
                false,
                '¡Por favor, verificar la información ingresada!',
                HttpResponse::HTTP_BAD_REQUEST,
                $data
            );
        }

        return $this->_comentarioService->ActualizarComentario($comentario, $request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Comentario  $comentario
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comentario $comentario)
    {
        return $this->_comentarioService->EliminarComentario($comentario);
    }
}
