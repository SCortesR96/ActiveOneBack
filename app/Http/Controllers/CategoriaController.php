<?php

namespace App\Http\Controllers;

use App\Models\Categoria;
use Illuminate\Http\Request;
use App\Services\CategoriaService;
use App\Services\Shared\JSONService;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Response as HttpResponse;

class CategoriaController extends Controller
{
    private JSONService $_jsonService;
    private CategoriaService $_categoriaService;

    public function __construct()
    {
        $this->_jsonService = new JSONService();
        $this->_categoriaService = new CategoriaService();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->_categoriaService->ListarCategorias();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Variables
        $data = [];
        $categoria = new Categoria();

        // Validación de Parametros
        $rules = [
            'nombre' => 'required|string|max:150'
        ];

        $messages = [
            'required' => 'La información del campo :attribute es requerida.',
            'max' => 'El campo :attribute no puede excender la cantidad de cáracteres especificada'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $data = [
                'errors' => $validator->errors()->all()
            ];

            return $this->jsonReponse = $this->_jsonService->JSONResponse(
                false,
                '¡Por favor, verificar la información ingresada!',
                HttpResponse::HTTP_BAD_REQUEST,
                $data
            );
        }

        return $this->_categoriaService->CrearCategoria($categoria, $request);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function show(Categoria $categoria)
    {
        return $this->_categoriaService->EliminarCategoria($categoria);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Categoria $categoria)
    {
        $data = [];

        $rules = [
            'nombre' => 'required|string|max:150',
            'estado' => 'required|integer|min:0|max:1'
        ];

        $messages = [
            'required' => 'La información del campo :attribute es requerida.',
            'max' => 'El campo :attribute no puede excender la cantidad de cáracteres especificada'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $data = [
                'errors' => $validator->errors()->all()
            ];

            return $this->jsonReponse = $this->_jsonService->JSONResponse(
                false,
                '¡Por favor, verificar la información ingresada!',
                HttpResponse::HTTP_BAD_REQUEST,
                $data
            );
        }

        return $this->_categoriaService->ActualizarCategoria($categoria, $request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function destroy(Categoria $categoria)
    {
        return $this->_categoriaService->EliminarCategoria($categoria);
    }
}
