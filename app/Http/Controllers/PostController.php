<?php

namespace App\Http\Controllers;

use Exception;
use Carbon\Carbon;
use App\Models\Post;
use App\Services\PostService;
use Illuminate\Http\Request;
use App\Services\Shared\LogService;
use App\Services\Shared\JSONService;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Response as HttpResponse;

class PostController extends Controller
{
    private JSONService $_jsonService;
    private PostService $_postService;

    public function __construct()
    {
        $this->_jsonService = new JSONService();
        $this->_postService = new PostService();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->_postService->ListarPosts();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [];

        $rules = [
            'categorias_id' => 'required|integer|min:1',
            'titulo' => 'required|string|max:150',
            'contenido' => 'required|string'
        ];

        $messages = [
            'required' => 'La información del campo ":attribute" es requerida.',
            'max' => 'El campo ":attribute" no puede excender la cantidad de cáracteres especificada'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $data = [
                'errors' => $validator->errors()->all()
            ];

            return $this->jsonReponse = $this->_jsonService->JSONResponse(
                false,
                '¡Por favor, verificar la información ingresada!',
                HttpResponse::HTTP_BAD_REQUEST,
                $data
            );
        }

        return $this->_postService->ListarPosts($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return $this->_postService->MostrarPost($post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $data = [];
        $message = '';
        $isValid = true;
        $codeResponse = HttpResponse::HTTP_OK;

        $rules = [
            'categorias_id' => 'required|integer|min:1',
            'titulo' => 'required|string|max:150',
            'contenido' => 'required|string',
            'estado' => 'required|integer|min:0|max:1'
        ];

        $messages = [
            'required' => 'La información del campo ":attribute" es requerida.',
            'max' => 'El campo ":attribute" no puede excender la cantidad de cáracteres especificada'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $data = [
                'errors' => $validator->errors()->all()
            ];

            return $this->jsonReponse = $this->_jsonService->JSONResponse(
                false,
                '¡Por favor, verificar la información ingresada!',
                HttpResponse::HTTP_BAD_REQUEST,
                $data
            );
        }

        return $this->_postService->ActualizarPost($post, $request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        return $this->_postService->EliminarPost($post);
    }
}
