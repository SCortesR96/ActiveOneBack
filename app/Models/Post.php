<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    protected $fillable = ['id', 'categorias_id', 'titulo', 'contenido', 'estado', 'anulado'];
    protected $visible = ['id', 'categorias_id', 'titulo', 'contenido', 'estado', 'fecha_creacion'];

    public $timestamps = false;

    // Relaciones entre modelos
    public function comentario()
    {
        return $this->hasMany(Comentario::class, 'id');
    }

    public function categoria()
    {
        return $this->belongsTo(Categoria::class, 'categorias_id');
    }
}
