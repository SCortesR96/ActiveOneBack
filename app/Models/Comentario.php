<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comentario extends Model
{
    use HasFactory;

    protected $fillable = ['id', 'posts_id', 'contenido', 'estado', 'anulado'];
    protected $visible = ['id', 'posts_id', 'contenido', 'estado', 'fecha_creacion'];

    public $timestamps = false;

    // Relaciones entre modelos
    public function post()
    {
        return $this->belongsTo(Post::class);
    }
}
