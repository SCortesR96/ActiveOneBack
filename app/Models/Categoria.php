<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    use HasFactory;

    protected $fillable = ['id', 'nombre', 'estado', 'anulado'];
    protected $visible  = ['id', 'nombre', 'estado', 'fecha_creacion'];

    public $timestamps = false;

    // Relaciones entre modelos
    public function post()
    {
        return $this->hasMany(Post::class, 'id');
    }
}
