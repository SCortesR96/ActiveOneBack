<?php

namespace App\Services;

use Exception;
use Carbon\Carbon;
use App\Models\Categoria;
use Illuminate\Http\Request;
use App\Services\Shared\JSONService;
use Illuminate\Http\Response as HttpResponse;

class CategoriaService
{
    private JSONService $_jsonService;

    function __construct()
    {
        $this->_jsonService = new JSONService();
    }

    function ListarCategorias()
    {
        $message = '';
        $isValid = true;
        $codeResponse = HttpResponse::HTTP_OK;

        try {
            $listarCategorias = Categoria::Where('anulado', 0)->Get();

            $data = ['categorias' => $listarCategorias];
            $message = 'Las categorías se han cargado correctamente.';
            $codeResponse = HttpResponse::HTTP_OK;
        } catch (Exception $e) {

            $data = [
                'clientMessage' => $this->_logService->getErrorMessage($e, 'categorias', 'Categoria.Index'),
            ];

            $isValid = false;
            $message = 'Ocurrió un error al mostrar las categorías.';
            $codeResponse = HttpResponse::HTTP_BAD_REQUEST;
        }

        return $this->_jsonService->JSONResponse(
            $isValid,
            $message,
            $codeResponse,
            $data
        );
    }

    function MostrarCategoria(Categoria $categoria)
    {
        $data = [];
        $message = '';
        $isValid = true;
        $codeResponse = HttpResponse::HTTP_OK;

        try {
            /*
                Obtenemos la categoría consultada por su id y aplicamos
                el filtro si es valida o no para mostrar según su estado
            */
            if ($categoria->anulado == 0) {
                if ($categoria->estado == 1) {
                    $data = ['categorias' => $categoria];
                    $message = 'La categoría ha cargado correctamente';
                } else {
                    $data = ['categorias' => []];
                    $codeResponse = HttpResponse::HTTP_NOT_FOUND;
                    $message = 'Esta categoría ha sido eliminada o deshabilitado.';
                }
            } else {
                $message = 'No se ha encontrado la categoría consultada.';
                $codeResponse = HttpResponse::HTTP_NOT_FOUND;
                $isValid = false;
            }
        } catch (Exception $e) {
            $data = [
                'clientMessage' => $this->_logService->GetErrorMessage($e, 'categorias', 'Categoria.Show')
            ];

            $codeResponse = HttpResponse::HTTP_BAD_REQUEST;
            $message = 'Ocurrió un error al cargar la categoría';
            $isValid = false;
        }

        return $this->_jsonService->JSONResponse(
            $isValid,
            $message,
            $codeResponse,
            $data

        );
    }

    function CrearCategoria(Categoria $categoria, Request $request)
    {
        $data = [];
        $message = '';
        $isValid = true;
        $codeResponse = HttpResponse::HTTP_OK;

        try {
            $categoria->nombre = $request->nombre;

            if ($categoria->save()) {
                $message = 'La categoría se ha creado correctamente.';
                $codeResponse = HttpResponse::HTTP_CREATED;
            } else {
                $message = 'Ocurrió un error al crear la categoría.';
                $codeResponse = HttpResponse::HTTP_BAD_REQUEST;
                $isValid = false;
            }
        } catch (Exception $e) {
            $data = [
                'clientMessage' => $this->_logService->GetErrorMessage($e, 'categorias', 'Categoria.Update')
            ];

            $codeResponse = HttpResponse::HTTP_BAD_REQUEST;
            $message = 'Ocurrió un error al crear la categoría';
            $isValid = false;
        }

        return $this->_jsonService->JSONResponse(
            $isValid,
            $message,
            $codeResponse,
            $data
        );
    }

    function ActualizarCategoria(Categoria $categoria, Request $request)
    {
        $data = [];
        $message = '';
        $isValid = true;
        $codeResponse = HttpResponse::HTTP_OK;

        try {
            if ($categoria->anulado == 1) {
                $message = 'La categoría ingresada no existe.';
                $codeResponse = HttpResponse::HTTP_BAD_REQUEST;
                $isValid = false;
            } else {
                $categoria->nombre = $request->nombre;
                $categoria->estado = $request->estado;
                $categoria->fecha_actualizacion = Carbon::now()->toDateTimeString();

                if ($categoria->save()) {
                    $message = 'La categoría se ha actualizado correctamente.';
                    $codeResponse = HttpResponse::HTTP_CREATED;
                } else {
                    $message = 'Ocurrió un error al actualizar la categoría.';
                    $codeResponse = HttpResponse::HTTP_BAD_REQUEST;
                    $isValid = false;
                }
            }
        } catch (Exception $e) {
            $data = [
                'clientMessage' => $this->_logService->GetErrorMessage($e, 'categorias', 'Categoria.Update')
            ];

            $codeResponse = HttpResponse::HTTP_BAD_REQUEST;
            $message = 'Ocurrió un error al actualizar la categoría';
            $isValid = false;
        }

        return $this->_jsonService->JSONResponse(
            $isValid,
            $message,
            $codeResponse,
            $data
        );
    }

    function EliminarCategoria(Categoria $categoria)
    {
        $data = [];
        $message = '';
        $isValid = true;
        $codeResponse = HttpResponse::HTTP_OK;

        try {
            if ($categoria->anulado == 0) {
                $categoria->anulado = 1;
                if ($categoria->save()) {
                    $message = 'La categoría se ha actualizado correctamente.';
                    $codeResponse = HttpResponse::HTTP_OK;
                } else {
                    $message = 'Ocurrió un error al eliminar esta categoría, por favor, intente más tarde.';
                    $codeResponse = HttpResponse::HTTP_BAD_REQUEST;
                    $isValid = false;
                }
            } else {
                $message = 'No se ha encontrado la categoría ingresada.';
                $codeResponse = HttpResponse::HTTP_NOT_FOUND;
                $isValid = false;
            }
        } catch (Exception $e) {
            $data = [
                'clientMessage' => $this->_logService->GetErrorMessage($e, 'categorias', 'Categoria.Update')
            ];

            $codeResponse = HttpResponse::HTTP_BAD_REQUEST;
            $message = 'Ocurrió un error al actualizar la categoría';
            $isValid = false;
        }

        return $this->_jsonService->JSONResponse(
            $isValid,
            $message,
            $codeResponse,
            $data
        );
    }
}
