<?php

namespace App\Services\Shared;

use Exception;
use Illuminate\Support\Facades\Log;

class LogService
{
    /*
    *   @Parametros:
            * e: Obtiene el error de tipo Exception.
            * channel: Obtiene el archivo en el cual se ingresará el error.
                + Esta configuración se encuentra en config/logging.php después de CUSTOM LOGS FILES.
                + La información que debe ir en channel debe ser [categorias, posts o comentarios].
            * location: Anotar el controlador y su función, ejemplo: CategoriaController.Show
    */
    public function GetErrorMessage(Exception $e, string $channel, string $location)
    {
        // Creamos la respuesta para el cliente y generamos la configuración del código personalizado para identificar el error
        $errorCode = `$channel-` . now()->timestamp;
        $errorClientMessage = "Ha ocurrido un error.\r\nContacte con un administrador, su código para una solución es: $errorCode";
        $errorLogMessage = "Error - $errorCode ($location):  \r\n" . $e->getMessage() . PHP_EOL;

        // Insertamos el error dentro del archivo configurado.
        Log::channel($channel)->error($errorLogMessage);

        return $errorClientMessage;
    }
}
