<?php

namespace App\Services\Shared;

class JSONService
{
    public function JSONResponse(bool $success, string $message, int $code, array $data)
    {
        return response()->json(
            [
                'success'   => $success,
                'message'   => $message,
                'code'      => $code,
                'data'      => $data
            ],
            $code
        );
    }
}
