<?php

namespace App\Services;

use Exception;
use Carbon\Carbon;
use App\Models\Post;
use Illuminate\Http\Request;
use App\Services\Shared\JSONService;
use Illuminate\Http\Response as HttpResponse;

class PostService
{
    private JSONService $_jsonService;

    function __construct()
    {
        $this->_jsonService = new JSONService();
    }

    function ListarPosts()
    {
        $data = [];
        $message = '';
        $isValid = true;
        $codeResponse = HttpResponse::HTTP_OK;

        try {
            $listarPosts = Post::Where('anulado', 0)->Get();

            $data = ['posts' => $listarPosts];
            $message = 'Los posts se han cargado correctamente.';
            $codeResponse = HttpResponse::HTTP_OK;
        } catch (Exception $e) {

            $data = [
                'clientMessage' => $this->_logService->getErrorMessage($e, 'posts', 'Post.Index'),
            ];

            $isValid = false;
            $message = 'Ocurrió un error al mostrar los posts.';
            $codeResponse = HttpResponse::HTTP_BAD_REQUEST;
        }

        return $this->_jsonService->JSONResponse(
            $isValid,
            $message,
            $codeResponse,
            $data
        );
    }

    function MostrarPost(Post $post)
    {
        $data = [];
        $message = '';
        $isValid = true;
        $codeResponse = HttpResponse::HTTP_OK;

        try {
            /*
                Obtenemos el post consultada por su id y aplicamos
                el filtro si es valida o no para mostrar según su estado
            */
            if ($post->anulado == 0) {
                if ($post->estado == 1) {
                    $data = ['posts' => $post];
                    $message = 'El post ha cargado correctamente';
                } else {
                    $data = ['posts' => []];
                    $codeResponse = HttpResponse::HTTP_NOT_FOUND;
                    $message = 'Este post ha sido deshabilitado.';
                }
            } else {
                $message = 'No se ha encontrado el post consultada.';
                $codeResponse = HttpResponse::HTTP_NOT_FOUND;
                $isValid = false;
            }
        } catch (Exception $e) {
            $data = [
                'clientMessage' => $this->_logService->GetErrorMessage($e, 'posts', 'Post.Show')
            ];

            $codeResponse = HttpResponse::HTTP_BAD_REQUEST;
            $message = 'Ocurrió un error al cargar el post';
            $isValid = false;
        }

        return $this->_jsonService->JSONResponse(
            $isValid,
            $message,
            $codeResponse,
            $data

        );
    }

    function CrearPost(Request $request)
    {
        $data = [];
        $message = '';
        $isValid = true;
        $codeResponse = HttpResponse::HTTP_OK;
        $post = new Post();

        try {
            $post->categorias_id = $request->categorias_id;
            $post->titulo = $request->titulo;
            $post->contenido = $request->contenido;

            if ($post->save()) {
                $message = 'El post se ha creado correctamente.';
                $codeResponse = HttpResponse::HTTP_CREATED;
            } else {
                $message = 'Ocurrió un error al crear el post.';
                $codeResponse = HttpResponse::HTTP_BAD_REQUEST;
                $isValid = false;
            }
        } catch (Exception $e) {
            $data = [
                'clientMessage' => $this->_logService->GetErrorMessage($e, 'posts', 'Post.Update')
            ];

            $codeResponse = HttpResponse::HTTP_BAD_REQUEST;
            $message = 'Ocurrió un error al crear el post';
            $isValid = false;
        }

        return $this->_jsonService->JSONResponse(
            $isValid,
            $message,
            $codeResponse,
            $data
        );
    }

    function ActualizarPost(Post $post, Request $request)
    {
        $data = [];
        $message = '';
        $isValid = true;
        $codeResponse = HttpResponse::HTTP_OK;

        try {
            if ($post->anulado == 1) {
                $message = 'El post ingresada no existe.';
                $codeResponse = HttpResponse::HTTP_BAD_REQUEST;
                $isValid = false;
            }

            $post->categorias_id = $request->categorias_id;
            $post->titulo = $request->titulo;
            $post->contenido = $request->contenido;
            $post->estado = $request->estado;
            $post->fecha_actualizacion = Carbon::now()->toDateTimeString();

            if ($post->save()) {
                $message = 'El post se ha actualizado correctamente.';
                $codeResponse = HttpResponse::HTTP_CREATED;
            } else {
                $message = 'Ocurrió un error al actualizar el post.';
                $codeResponse = HttpResponse::HTTP_BAD_REQUEST;
                $isValid = false;
            }
        } catch (Exception $e) {
            $data = [
                'clientMessage' => $this->_logService->GetErrorMessage($e, 'posts', 'Post.Update')
            ];

            $codeResponse = HttpResponse::HTTP_BAD_REQUEST;
            $message = 'Ocurrió un error al actualizar el post';
            $isValid = false;
        }

        return $this->_jsonService->JSONResponse(
            $isValid,
            $message,
            $codeResponse,
            $data
        );
    }

    function EliminarPost(Post $post)
    {
        $data = [];
        $message = '';
        $isValid = true;
        $codeResponse = HttpResponse::HTTP_OK;

        try {
            if ($post->anulado == 0) {
                $post->anulado = 1;
                if ($post->save()) {
                    $message = 'El post se ha eliminado correctamente.';
                    $codeResponse = HttpResponse::HTTP_OK;
                } else {
                    $message = 'Ocurrió un error al eliminar este post, por favor, intente más tarde.';
                    $codeResponse = HttpResponse::HTTP_BAD_REQUEST;
                    $isValid = false;
                }
            } else {
                $message = 'No se ha encontrado el post ingresada.';
                $codeResponse = HttpResponse::HTTP_NOT_FOUND;
                $isValid = false;
            }
        } catch (Exception $e) {
            $data = [
                'clientMessage' => $this->_logService->GetErrorMessage($e, 'posts', 'Post.Update')
            ];

            $codeResponse = HttpResponse::HTTP_BAD_REQUEST;
            $message = 'Ocurrió un error al actualizar el post';
            $isValid = false;
        }

        return $this->_jsonService->JSONResponse(
            $isValid,
            $message,
            $codeResponse,
            $data
        );
    }
}
