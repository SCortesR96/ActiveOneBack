<?php

namespace App\Services;

use Exception;
use Carbon\Carbon;
use App\Models\Comentario;
use Illuminate\Http\Request;
use App\Services\Shared\JSONService;
use Illuminate\Http\Response as HttpResponse;

class ComentarioService
{
    private JSONService $_jsonService;

    function __construct()
    {
        $this->_jsonService = new JSONService();
    }

    function ListarComentarios()
    {
        $message = '';
        $isValid = true;
        $codeResponse = HttpResponse::HTTP_OK;

        try {
            $listarComentarios = Comentario::Where('anulado', 0)->Get();

            $data = ['comentarios' => $listarComentarios];
            $message = 'Los comentarios se han cargado correctamente.';
            $codeResponse = HttpResponse::HTTP_OK;
        } catch (Exception $e) {

            $data = [
                'clientMessage' => $this->_logService->getErrorMessage($e, 'comentarios', 'Comentario.Index'),
            ];

            $isValid = false;
            $message = 'Ocurrió un error al mostrar los comentarios.';
            $codeResponse = HttpResponse::HTTP_BAD_REQUEST;
        }

        return $this->_jsonService->JSONResponse(
            $isValid,
            $message,
            $codeResponse,
            $data
        );
    }

    function MostrarComentario(Comentario $comentario)
    {
        $data = [];
        $message = '';
        $isValid = true;
        $codeResponse = HttpResponse::HTTP_OK;

        try {
            /*
                Obtenemos el comentario consultada por su id y aplicamos
                el filtro si es valida o no para mostrar según su estado
            */
            if ($comentario->anulado == 0) {
                if ($comentario->estado == 1) {
                    $data = ['comentarios' => $comentario];
                    $message = 'El comentario ha cargado correctamente';
                } else {
                    $data = ['comentarios' => []];
                    $codeResponse = HttpResponse::HTTP_NOT_FOUND;
                    $message = 'Este comentario ha sido deshabilitado.';
                }
            } else {
                $message = 'No se ha encontrado el comentario consultada.';
                $codeResponse = HttpResponse::HTTP_NOT_FOUND;
                $isValid = false;
            }
        } catch (Exception $e) {
            $data = [
                'clientMessage' => $this->_logService->GetErrorMessage($e, 'comentarios', 'Comentario.Show')
            ];

            $codeResponse = HttpResponse::HTTP_BAD_REQUEST;
            $message = 'Ocurrió un error al cargar el comentario';
            $isValid = false;
        }

        return $this->_jsonService->JSONResponse(
            $isValid,
            $message,
            $codeResponse,
            $data

        );
    }

    function CrearComentario(Request $request)
    {
        $data = [];
        $message = '';
        $isValid = true;
        $codeResponse = HttpResponse::HTTP_OK;
        $comentario = new Comentario();

        try {
            $comentario->posts_id = $request->posts_id;
            $comentario->contenido = $request->contenido;

            if ($comentario->save()) {
                $message = 'El comentario se ha creado correctamente.';
                $codeResponse = HttpResponse::HTTP_CREATED;
            } else {
                $message = 'Ocurrió un error al crear el comentario.';
                $codeResponse = HttpResponse::HTTP_BAD_REQUEST;
                $isValid = false;
            }
        } catch (Exception $e) {
            $data = [
                'clientMessage' => $this->_logService->GetErrorMessage($e, 'comentarios', 'Comentario.Update')
            ];

            $codeResponse = HttpResponse::HTTP_BAD_REQUEST;
            $message = 'Ocurrió un error al crear el comentario';
            $isValid = false;
        }

        return $this->_jsonService->JSONResponse(
            $isValid,
            $message,
            $codeResponse,
            $data
        );
    }

    function ActualizarComentario(Comentario $comentario, Request $request)
    {
        $data = [];
        $message = '';
        $isValid = true;
        $codeResponse = HttpResponse::HTTP_OK;

        try {
            if ($comentario->anulado == 1) {
                $message = 'El comentario ingresada no existe.';
                $codeResponse = HttpResponse::HTTP_BAD_REQUEST;
                $isValid = false;
            }

            $comentario->posts_id = $request->posts_id;
            $comentario->contenido = $request->contenido;
            $comentario->estado = $request->estado;
            $comentario->fecha_actualizacion = Carbon::now()->toDateTimeString();

            if ($comentario->save()) {
                $message = 'El comentario se ha actualizado correctamente.';
                $codeResponse = HttpResponse::HTTP_CREATED;
            } else {
                $message = 'Ocurrió un error al actualizar el comentario.';
                $codeResponse = HttpResponse::HTTP_BAD_REQUEST;
                $isValid = false;
            }
        } catch (Exception $e) {
            $data = [
                'clientMessage' => $this->_logService->GetErrorMessage($e, 'comentarios', 'Comentario.Update')
            ];

            $codeResponse = HttpResponse::HTTP_BAD_REQUEST;
            $message = 'Ocurrió un error al actualizar el comentario';
            $isValid = false;
        }

        return $this->_jsonService->JSONResponse(
            $isValid,
            $message,
            $codeResponse,
            $data
        );
    }

    function EliminarComentario(Comentario $comentario)
    {
        $data = [];
        $message = '';
        $isValid = true;
        $codeResponse = HttpResponse::HTTP_OK;

        try {
            if ($comentario->anulado == 0) {
                $comentario->anulado = 1;
                if ($comentario->save()) {
                    $message = 'El comen$comentario se ha eliminado correctamente.';
                    $codeResponse = HttpResponse::HTTP_OK;
                } else {
                    $message = 'Ocurrió un error al eliminar este comen$comentario, por favor, intente más tarde.';
                    $codeResponse = HttpResponse::HTTP_BAD_REQUEST;
                    $isValid = false;
                }
            } else {
                $message = 'No se ha encontrado el comen$comentario ingresada.';
                $codeResponse = HttpResponse::HTTP_NOT_FOUND;
                $isValid = false;
            }
        } catch (Exception $e) {
            $data = [
                'clientMessage' => $this->_logService->GetErrorMessage($e, 'posts', 'Comentario.Update')
            ];

            $codeResponse = HttpResponse::HTTP_BAD_REQUEST;
            $message = 'Ocurrió un error al actualizar el comen$comentario';
            $isValid = false;
        }

        return $this->_jsonService->JSONResponse(
            $isValid,
            $message,
            $codeResponse,
            $data
        );
    }
}
