# Activeone Solutions S.A.S.

> ### _Instructivo de prueba técnica de Sergio Cortés_

<br>

## Tabla de contenido

1. [Requisitos mínimos](#requisitos)
2. [Entorno y herramientas](#entorno)
3. [Descarga e instalación del proyecto](#instalacion)
4. [Creación de la base de datos](#crear_base_datos)
5. [Migración a la base de datos](#migracion)
6. [Ejecución del servidor](#correr_servidor)
7. [Environment en postman](#environment_postman)
8. [Importación y pruebas de servicios](#pruebas)

<div id="requisitos" style="padding-top: 3rem;">

## 1. Requisitos mínimos

Los requisitos mínimos para ejecutar este proyecto satisfactoriamente son:

    -   PHP 7.3
    -   Composer
    -   MySQL

</div>

<div id="entorno" style="padding-top: 2rem;">

## 2. Entorno y herramientas

Para esta prueba se han utilizado las siguientes herramientas:

<ul>
    <li>Visual Studio Code <a href="https://code.visualstudio.com/download">(Descargar)</a></li>
    <li>Composer <a href="https://getcomposer.org/download/">(Descargar)</a></li>
    <li>PostgreSQL <a href="https://www.postgresql.org/download/windows/">(Descargar)</a></li>
    <li>DBeaver <a href="https://dbeaver.io/download/">(Descargar)</a></li>
    <li>Postman <a href="https://www.postman.com/downloads/">(Descargar)</a></li>
    <li>Git <a href="https://git-scm.com/download/win">(Descargar)</a></li>
</ul>

</div>

<div id="instalacion" style="padding-top: 2rem;">

## 3. Descarga e instalación del proyecto

-   Nos dirigimos al <a href="https://gitlab.com/SCortesR96/ActiveOneBack">repositorio</a> donde se encuentra almacenado la prueba técnica.
-   Procedemos a copiar la URL de HTTPS para realizar un **git clone**. <br>
    ![Repositorio](https://i.postimg.cc/7Zm0T3YL/paso1.png)
    <br><br>
-   Abrimos una consola (a preferencia del usuario, en este caso se usó git bash) e ingresamos el comando _git clone_. <br>
    ![Git Clone](https://i.postimg.cc/nLr7KXCG/paso2.png)
    <br><br>
-   Una vez que tengamos clonado nuestro repositorio ingresamos a la carpeta e instalamos los paquetes del proyecto con el comando **_composer install_**. <br>
![Composer install](https://i.postimg.cc/KjPgcLHx/paso3.png) <br>
![Completed installation](https://i.postimg.cc/FRJLqwbf/paso4.png)
</div>

<div id="crear_base_datos" style="padding-top: 2rem;">

## 4. Creación de la base de datos

Para crear nuestra base de datos de prueba accedemos a nuestro gestor de base de datos y usamos el siguiente script:

    * CREATE DATABASE `activeone`;

</div>

<div id="migracion" style="padding-top: 2rem;">

## 5. Migración a la base de datos

Para la migración de nuestra base de datos volvemos a nuestra consola y ejecutamos el comando **_php artisan migrate:refresh --seed_**.

<br>

-   **php artisan:** Es el comando utilizado para utilizar todas las funciones de laravel (migrar, crear, publicar, ver rutas, entre otras opciones).

-   **migrate:refresh:** esta parte se divide en 2. La primera parte es _migrate_ y se utiliza para migrar todas las tablas a la base de datos. La segunda parte es _refresh_, esta se utiliza para ejecutar un rollback completo a la base de datos para que esta migración se ejecute sin conflictos.

-   **--seed:** Este comando lo utilizamos para ejecutar nuestros **Factories** y crear la información falsa e insertarla en las tablas de la base de datos.

</div>

<div id="correr_servidor" style="padding-top: 2rem;">

## 6. Ejecución del servidor

Una vez completados los pasos anteriores nos dirigimos a la consola, y dentro del proyecto ejecutamos el comando **_php artisan serve_** para poner en funcionamiento el servidor del proyecto.

</div>

<div id="environment_postman" style="padding-top: 2rem;">

## 7. Environment en postman

-   Primeramente, abrimos Postman y nos dirigmos a la opción de _Environment quick look_ (lo encontramos en la parte superior derecha), una vez que elegimos esa opción, damos click en _add_. <br>
    ![Environment postman](https://i.postimg.cc/CMk84TLX/paso5.png)

-   Asignamos un nombre al Environment con el que vamos a trabajar para las pruebas de los servicios, una vez completado, asignamos un nombre a la variable de ese Environment y el valor, en este caso como es un entorno local, asignamos el valor de **http://127.0.0.1:8000/api**. <br>
    ![Variable Environment](https://i.postimg.cc/d0fC5JCP/paso6.png)

</div>

<div id="pruebas" style="padding-top: 2rem;">

## 8. Importación y pruebas de servicios

Para poder realizar las respectivas las respectivas pruebas de cada servicio, procedemos a importar el recurso JSON en Postman que se adjuntó en el correo enviado.

1.  Importamos el recurso presionando _Ctrl+O_ y elegimos el archivo _.json_ que descargamos anteriormente. <br>
    ![Recurso JSON](https://i.postimg.cc/0jRwqZZV/paso7.png)
2.  Elegimos el Environment y comenzamos a realizar nuestras pruebas con los servicios importados. <br>
![Pruebas](https://i.postimg.cc/28YZcc16/paso8.png)
</div>
