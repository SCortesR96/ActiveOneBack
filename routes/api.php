<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'categorias'], function () {
    Route::Get('', 'CategoriaController@index');
    Route::Post('', 'CategoriaController@store');
    Route::Get('/{categoria}', 'CategoriaController@show');
    Route::Put('/{categoria}', 'CategoriaController@update');
    Route::Post('/{categoria}', 'CategoriaController@destroy');
});

Route::group(['prefix' => 'posts'], function () {
    Route::Get('', 'PostController@index');
    Route::Post('', 'PostController@store');
    Route::Get('/{post}', 'PostController@show');
    Route::Put('/{post}', 'PostController@update');
    Route::Post('/{post}', 'PostController@destroy');
});

Route::group(['prefix' => 'comentarios'], function () {
    Route::Get('', 'ComentarioController@index');
    Route::Post('', 'ComentarioController@store');
    Route::Get('/{comentario}', 'ComentarioController@show');
    Route::Put('/{comentario}', 'ComentarioController@update');
    Route::Post('/{comentario}', 'ComentarioController@destroy');
});
